Here be dragons. This shouldn't go to the master branch.
.
This branch rips out PO's heart and transplants it into a Frankenstein-like
testing machinery.
.
I might find some bugs this way.
.
The commits are meant more as a set of patches than history
snapshots, and so they'll get rebased a lot.
.
...