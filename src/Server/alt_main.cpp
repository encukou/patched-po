#include <QtCore/QCoreApplication>
#include <QtCore/QSettings>
#include <QtNetwork/QTcpSocket>

#include "player.h"
#include "battle.h"
#include "pluginmanager.h"
#include "server.h"
#include "../PokemonInfo/pokemoninfo.h"
#include "../Utilities/contextswitch.h"
#include "../Shared/battlecommands.h"
#include "moves.h"
#include "abilities.h"
#include "items.h"
#include "analyze.h"

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cstdio>


#include <execinfo.h>

using namespace BattleCommands;

/* Obtain a backtrace and print it to stdout. */
void print_trace (void) {
    void *array[10];
    size_t size;
    char **strings;
    size_t i;

    size = backtrace (array, 10);
    strings = backtrace_symbols (array, size);

    fprintf (stderr, "Obtained %zd stack frames.\n", size);

    for (i = 0; i < size; i++)
        fprintf (stderr, "%s\n", strings[i]);

    free (strings);
}

FILE* _DAMN_INPUT_FILE_;

template<class T>
T get_value(QString description) {
    T thing;
    std::cout << "G:" << description.toStdString() << std::endl << std::flush;
    std::cin >> thing;
    char str[101];
    std::cin.getline(str, 101);
    return thing;
}

template<>
QString get_value<QString>(QString description) {
    std::cout << "G:" << description.toStdString() << std::endl << std::flush;
    char str[101];
    std::cin.getline(str, 101);
    return str;
}

void fnExit(void) {
    qDebug() << "PO TERMINATING!";
}

int main(int argc, char *argv[]) {
    atexit(fnExit);

    PokemonInfo::init();
    MoveInfo::init();
    ItemInfo::init();
    TypeInfo::init();
    NatureInfo::init();
    AbilityInfo::init();
    GenderInfo::init();
    StatInfo::init();

    MoveEffect::init();
    AbilityEffect::init();
    ItemEffect::init();

    std::cerr << "No. of args: " << argc << std::endl;
    std::cerr << "Input file descriptor: " << argv[1] << std::endl;

    _DAMN_INPUT_FILE_ = fdopen(atoi(argv[1]), "r");

    std::cerr << "Got input" << std::endl;

    ContextSwitcher battleThread;

    /* Names to use later for QSettings */
    QCoreApplication::setApplicationName("Checker for Pogeymon-Online");
    QCoreApplication::setOrganizationName("ReGeneration DevTeam");

    QSettings s("config", QSettings::IniFormat);

    s.setValue("mainchanname", "Stratocumulus laboratory");

    //in headless mode let's use QCoreApplication instead of QApplication
    QCoreApplication b(argc, argv);

    QTcpSocket dummy_sock;
    Player *players[2];


    for(int p=0; p<2; p++) {
        TeamInfo teamInfo;
        teamInfo.name = QString("Trainer %1").arg(p);
        for(int i=0; i<6; i++) {
            teamInfo.m_pokes[i].m_prop_nickname = get_value<QString>("Nickname");
            teamInfo.m_pokes[i].m_prop_num = get_value<int>("Species");
            teamInfo.m_pokes[i].m_prop_item = get_value<int>("Item");
            teamInfo.m_pokes[i].m_prop_ability = get_value<int>("Ability");
            std::cerr << "!!ABI!!:" << teamInfo.m_pokes[i].ability() << std::endl;
            teamInfo.m_pokes[i].m_prop_nature = get_value<int>("Nature");
            std::cerr << "!!NAT!!:" << int(teamInfo.m_pokes[i].nature()) << std::endl;
            teamInfo.m_pokes[i].m_prop_gender = get_value<int>("Gender");
            teamInfo.m_pokes[i].m_prop_shiny = get_value<bool>("Shiny");
            teamInfo.m_pokes[i].m_prop_happiness = get_value<int>("Tameness");
            teamInfo.m_pokes[i].m_prop_level = get_value<int>("Level");
            teamInfo.m_pokes[i].m_prop_gen = get_value<int>("Gen");
            for(int m=0; m<4; m++) teamInfo.m_pokes[i].m_moves[m] = get_value<int>("Move");
            for(int m=0; m<6; m++) teamInfo.m_pokes[i].m_DVs[m] = get_value<int>("Gene");
            for(int m=0; m<6; m++) teamInfo.m_pokes[i].m_EVs[m] = get_value<int>("Effort");
        }
        std::cerr << "!!ABI!!:" << teamInfo.m_pokes[0].ability() << std::endl;
        players[p] = new Player(&dummy_sock, p);
        players[p]->m_prop_winningMessage = "YES!";
        players[p]->m_prop_losingMessage = "NO!";
        std::cerr << "!!ABI!!:" << teamInfo.m_pokes[0].ability() << std::endl;
        players[p]->assignTeam(teamInfo);
        std::cerr << "!!ABI!!:" << teamInfo.m_pokes[0].ability() << std::endl;
        std::cerr << "!!ABI!!:" << players[p]->team().poke(0).ability() << std::endl;
        std::cerr << "!!NAT!!:" << int(players[p]->team().poke(0).nature()) << std::endl;
    }

    std::cerr << "!!ABI!!:" << players[0]->team().poke(0).ability() << std::endl;

    ChallengeInfo c;
    c.clauses = 0;
    c.rated = 0;

    Server server;

    PluginManager pluginManager(&server);

    int id = 0;

    BattleSituation battle(*players[0], *players[1], c, id, &pluginManager);
    battle.useBattleLog = false;

    players[0]->startBattle(id, 0, battle.pubteam(1), battle.configuration());
    players[1]->startBattle(id, 1, battle.pubteam(0), battle.configuration());

    battleThread.start();

    battle.start(battleThread);

    std::cerr << "Entering main loop" << std::endl;

    QCoreApplication::exec();

    std::cerr << "Finished! A CLEAN EXIT!!" << std::endl;
}

void BattleSituation::battleChoiceReceived(const QByteArray &str) {
    qDebug() << "Received canned battle choice " << QString::fromUtf8(str).trimmed();
    QByteArray ba = QByteArray::fromBase64(str);
    QDataStream ds((const QByteArray)ba);
    BattleChoice po;
    ds >> po;
    battleChoiceReceived(po.slot(), po);
}

void BattleSituation::battleInfo(int publicId, int id, const QByteArray &info) {
    switch(info[0]) {
        case BlankMessage:
        case Spectating:
        case OfferChoice:
        case DynamicInfo:
        case DynamicStats:
        case ClockStart:
        case ClockStop:
        case Ko:
        case StraightDamage:
        case Rated:
        case AbsStatusChange:
            qDebug() << "Sending an ignored battle command" << int(info[0]);
            return;
        case StartChoices: {
            std::cerr << "Getting battle command " << int(info[0]) << std::endl;
            std::cerr << "(expecting answer on alt channel!)" << std::endl;
            char str[101];
            fgets(str, 101, _DAMN_INPUT_FILE_);
            std::cerr << "Got " << str << std::endl;
            QMetaObject::invokeMethod(
                    this,
                    "battleChoiceReceived",
                    Qt::QueuedConnection,
                    Q_ARG(QByteArray, str)
                );
        } break;
        case Effective:
            if(info[2] == 4) {
                qDebug() << "Normal effectivity not sent" << int(info[0]);
                return;
            }
            // else fallback
        default: {
            std::cerr << "Sending battle command " << int(info[0]) << std::endl;
            std::cout << "B:" << publicId << ":" << id << ":$" << info.toBase64().data() << std::endl;
            std::cerr << "Waiting for OK" << std::endl;
            char str[101];
            std::cin.getline(str, 101);
            if(!std::cin) {
                std::cerr << "No stdin!! Bailing" << std::endl;
                ::exit(3);
            }
            if(QString("OK") != QString(str).trimmed()) {
                std::cout << "No OK received; got '" << str << "'" << std::endl;
                exit();
            }else{
                std::cerr << "PO: Got OK" << std::endl;
            }
        } break;
    }
}

void Analyzer::sendCommand(const QByteArray &command) {
    std::cout << "C:$" << command.toBase64().data() << std::endl;
}

#undef randint
#undef coinflip

unsigned BattleSituation::randint(unsigned max, QString f, int l) const {
    if (max == 1) return 0;
    qDebug() << "Rand int: 0.." << (max - 1) << "loc:" << f << l;
    std::cout << "R:I:" << (max - 1) << std::endl;
    char str[101];
    std::cin.getline(str, 101);
    QString qstr(str);
    int i = qstr.toInt();
    qDebug() << "Rand int: 0.." << (max - 1) << "is" << i;
    return i;
}

int gcd_euclid(int a, int b) {
    if(b == 0) return a;
    return gcd_euclid(b, a % b);
}

bool BattleSituation::coinflip(unsigned a, unsigned b, QString f, int l) const {
    qDebug() << "Coin flip: " << a << "/" << b << "loc:" << f << l;
    int gcd = gcd_euclid(a, b);
    if (a == 0) return false;
    a /= gcd;
    b /= gcd;
    std::cout << "R:C:" << a << ":" << b << std::endl;
    char str[101];
    std::cin.getline(str, 101);
    bool boo = (str[0] == 'T');
    qDebug() << "Coin flip: " << a << "/" << b << "is" << boo;
    return boo;
}
